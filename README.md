# Calo Test Task

Task was completed using react native.

UI Kitten was used as UI kit to have fundamental components
with basic design out of the box.

Optional tasks were also implemented 
(list infinite loading, map loading only items within current viewport)

How to run:
```
# install dependencies
yarn install

# install pods
npx pod-install ios

# run app
npx react-native run-ios
npx react-native run-android
```
