import { Button, Card, Text, useTheme } from '@ui-kitten/components';
import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { Delivery } from '../types';

interface Props {
  delivery: Delivery;
  onStatusChange: (id: string, status: 'delivering' | 'delivered') => void;
}

const DeliveryCard: FC<Props> = ({ delivery, onStatusChange }) => {
  const theme = useTheme();

  const handleStatusChange = () => {
    onStatusChange(
      delivery.id,
      !delivery.deliveryStatus ? 'delivering' : 'delivered',
    );
  };

  return (
    <Card disabled>
      <View style={styles.headingRow}>
        <Text category="h6" style={styles.name}>
          {delivery.name}
        </Text>
        <View
          style={[
            styles.statusBadge,
            { backgroundColor: theme['color-primary-200'] },
          ]}>
          <Text category="label">
            {delivery.deliveryStatus
              ? delivery.deliveryStatus.toUpperCase()
              : 'PENDING'}
          </Text>
        </View>
      </View>
      <Text category="p1" style={styles.address}>
        {delivery.address}
      </Text>
      {delivery.deliveryStatus !== 'delivered' ? (
        <Button
          size="small"
          onPress={handleStatusChange}
          style={styles.actionButton}>
          {delivery.deliveryStatus === 'delivering'
            ? 'Mark as delivered'
            : 'Mark as delivering'}
        </Button>
      ) : null}
    </Card>
  );
};

const styles = StyleSheet.create({
  headingRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  name: {
    marginRight: 10,
    marginBottom: 10,
  },
  statusBadge: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderRadius: 4,
  },
  address: {
    marginTop: 8,
    marginBottom: 5,
  },
  actionButton: {
    marginTop: 10,
    alignSelf: 'flex-start',
  },
});

export default React.memo(DeliveryCard);
