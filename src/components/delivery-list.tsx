import { List } from '@ui-kitten/components';
import React, { FC, useCallback, useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import useDeliveryService from '../services/hooks/use-delivery-service';
import { Delivery } from '../types';
import DeliveryCard from './delivery-card';

interface Props {
  filter: 'new' | 'delivered';
  setDeliveryCounts: (
    callback: (
      prevCounts: {
        total: number;
        delivered: number;
      } | null,
    ) => { total: number; delivered: number } | null,
  ) => void;
}

const PAGE_LIMIT = 10;

const DeliveryList: FC<Props> = ({ filter, setDeliveryCounts }) => {
  const deliveryService = useDeliveryService();

  const [deliveries, setDeliveries] = useState<Delivery[]>([]);
  const [isLoading, setLoading] = useState(false);
  const [from, setFrom] = useState(0);
  const [count, setCount] = useState(0);

  useEffect(() => {
    const fetchFirstPage = async () => {
      try {
        setLoading(true);
        const response = await deliveryService.fetchDeliveries(
          filter,
          0,
          PAGE_LIMIT,
        );
        setDeliveries(response.data);
        setCount(response.count);
        setFrom(PAGE_LIMIT);
      } catch (e) {
        console.error(e.message);
      } finally {
        setLoading(false);
      }
    };

    fetchFirstPage();
  }, [filter, deliveryService]);

  const onLoadMore = async () => {
    if (deliveries.length < count && !isLoading) {
      try {
        setLoading(true);
        const response = await deliveryService.fetchDeliveries(
          filter,
          from,
          PAGE_LIMIT,
        );
        setDeliveries(prevDeliveries => [...prevDeliveries, ...response.data]);
        setCount(response.count);
        setFrom(prevFrom => prevFrom + PAGE_LIMIT);
      } catch (e) {
        console.error(e.message);
      } finally {
        setLoading(false);
      }
    }
  };

  const onStatusChange = useCallback(
    async (id: string, deliveryStatus: 'delivering' | 'delivered') => {
      if (deliveryStatus === 'delivered') {
        setDeliveries(prevDeliveries =>
          prevDeliveries.filter(prevDelivery => prevDelivery.id !== id),
        );

        setDeliveryCounts(prevCounts => {
          if (prevCounts) {
            return {
              ...prevCounts,
              delivered: prevCounts.delivered + 1,
            };
          }
          return prevCounts;
        });
      } else {
        setDeliveries(prevDeliveries =>
          prevDeliveries.map(prevDelivery => {
            if (prevDelivery.id === id) {
              return { ...prevDelivery, deliveryStatus };
            }
            return prevDelivery;
          }),
        );
      }

      await deliveryService.editDeliveryStatus(id, deliveryStatus);
    },
    [deliveryService, setDeliveryCounts],
  );

  return (
    <List
      data={deliveries}
      keyExtractor={item => item.id}
      renderItem={({ item }) => (
        <DeliveryCard delivery={item} onStatusChange={onStatusChange} />
      )}
      ListFooterComponent={
        isLoading ? (
          <ActivityIndicator size="small" style={styles.spinner} />
        ) : null
      }
      onEndReachedThreshold={0.2}
      onEndReached={onLoadMore}
    />
  );
};

const styles = StyleSheet.create({
  spinner: {
    marginTop: 15,
  },
});

export default DeliveryList;
