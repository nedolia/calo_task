import React, { FC, useCallback, useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE, Region } from 'react-native-maps';
import useDeliveryService from '../services/hooks/use-delivery-service';
import { Delivery, RegionBoundary } from '../types';
import DeliveryCard from './delivery-card';

interface Props {
  filter: 'new' | 'delivered';
  setDeliveryCounts: (
    callback: (
      prevCounts: {
        total: number;
        delivered: number;
      } | null,
    ) => { total: number; delivered: number } | null,
  ) => void;
}

const DeliveryMap: FC<Props> = ({ filter, setDeliveryCounts }) => {
  const deliveryService = useDeliveryService();

  const [selectedDelivery, setSelectedDelivery] = useState<Delivery | null>(
    null,
  );
  const [deliveries, setDeliveries] = useState<Delivery[]>([]);
  const [regionBoundary, setRegionBoundary] = useState<RegionBoundary | null>(
    null,
  );

  useEffect(() => {
    const fetchDeliveries = async () => {
      if (regionBoundary) {
        const response = await deliveryService.fetchDeliveriesInBoundary(
          filter,
          regionBoundary,
        );
        setDeliveries(response.data);
      }
    };

    fetchDeliveries();
  }, [deliveryService, filter, regionBoundary]);

  const onRegionChangeComplete = (region: Region) => {
    setRegionBoundary({
      westLng: region.longitude - region.longitudeDelta / 2,
      southLat: region.latitude - region.latitudeDelta / 2,
      eastLng: region.longitude + region.longitudeDelta / 2,
      northLat: region.latitude + region.latitudeDelta / 2,
    });
  };

  const onStatusChange = useCallback(
    async (id: string, deliveryStatus: 'delivering' | 'delivered') => {
      if (deliveryStatus === 'delivered') {
        setDeliveries(prevDeliveries =>
          prevDeliveries.filter(prevDelivery => prevDelivery.id !== id),
        );

        setDeliveryCounts(prevCounts => {
          if (prevCounts) {
            return {
              ...prevCounts,
              delivered: prevCounts.delivered + 1,
            };
          }
          return prevCounts;
        });

        setSelectedDelivery(null);
      } else {
        setDeliveries(prevDeliveries =>
          prevDeliveries.map(prevDelivery => {
            if (prevDelivery.id === id) {
              return { ...prevDelivery, deliveryStatus };
            }
            return prevDelivery;
          }),
        );

        setSelectedDelivery(prevSelected => {
          if (prevSelected) {
            return { ...prevSelected, deliveryStatus };
          }
          return prevSelected;
        });
      }

      await deliveryService.editDeliveryStatus(id, deliveryStatus);
    },
    [deliveryService, setDeliveryCounts],
  );

  return (
    <View>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        onRegionChangeComplete={onRegionChangeComplete}>
        {deliveries.map(delivery => (
          <Marker
            key={delivery.id}
            coordinate={{ latitude: delivery.lat, longitude: delivery.lng }}
            onPress={() => setSelectedDelivery(delivery)}
          />
        ))}
      </MapView>
      {selectedDelivery ? (
        <View style={styles.selectedDelivery}>
          <DeliveryCard
            delivery={selectedDelivery}
            onStatusChange={onStatusChange}
          />
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
    height: '100%',
  },
  selectedDelivery: {
    position: 'absolute',
    bottom: 30,
    left: 10,
    right: 10,
  },
});

export default DeliveryMap;
