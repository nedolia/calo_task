import {
  Button,
  Icon,
  IndexPath,
  MenuItem,
  OverflowMenu,
  TopNavigation,
} from '@ui-kitten/components';
import React, { FC, useState } from 'react';
import { StyleSheet } from 'react-native';

interface Props {
  deliveryCounts: { total: number; delivered: number } | null;
  viewMode: 'list' | 'map';
  onViewModeChange: (mode: 'list' | 'map') => void;
  filter: 'new' | 'delivered';
  onFilterChange: (filter: 'new' | 'delivered') => void;
}

const NavigationBar: FC<Props> = ({
  deliveryCounts,
  viewMode,
  onViewModeChange,
  filter,
  onFilterChange,
}) => {
  const [filterVisible, setFilterVisible] = useState(false);

  const toggleViewMode = () => {
    onViewModeChange(viewMode === 'list' ? 'map' : 'list');
  };

  const toggleFilter = (index: IndexPath) => {
    onFilterChange(index.row === 0 ? 'new' : 'delivered');
    setFilterVisible(false);
  };

  const title = deliveryCounts
    ? `${deliveryCounts.delivered}/${deliveryCounts.total} delivered`
    : '';

  return (
    <TopNavigation
      title={title}
      alignment="center"
      accessoryLeft={buttonProps => (
        <Button
          {...buttonProps}
          onPress={toggleViewMode}
          style={styles.navigationButton}
          appearance="ghost"
          accessoryLeft={iconProps => (
            <Icon
              {...iconProps}
              style={[iconProps?.style, styles.navigationIcon]}
              name={viewMode === 'list' ? 'pin-outline' : 'list-outline'}
            />
          )}
        />
      )}
      accessoryRight={buttonProps => (
        <OverflowMenu
          visible={filterVisible}
          onSelect={toggleFilter}
          anchor={() => (
            <Button
              {...buttonProps}
              onPress={() => setFilterVisible(true)}
              style={styles.navigationButton}
              appearance="ghost"
              accessoryLeft={iconProps => (
                <Icon
                  {...iconProps}
                  style={[iconProps?.style, styles.navigationIcon]}
                  name="funnel-outline"
                />
              )}
            />
          )}
          onBackdropPress={() => setFilterVisible(false)}>
          <MenuItem title="New" selected={filter === 'new'} />
          <MenuItem title="Delivered" selected={filter === 'delivered'} />
        </OverflowMenu>
      )}
    />
  );
};

const styles = StyleSheet.create({
  navigationButton: {
    paddingHorizontal: 0,
    paddingVertical: 0,
  },
  navigationIcon: {
    width: 20,
    height: 20,
  },
});

export default NavigationBar;
