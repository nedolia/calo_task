import { Layout } from '@ui-kitten/components';
import React, { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import DeliveryList from '../components/delivery-list';
import DeliveryMap from '../components/delivery-map';
import NavigationBar from '../components/navigation-bar';
import useDeliveryService from '../services/hooks/use-delivery-service';

const HomePage = () => {
  const deliveryService = useDeliveryService();

  const [viewMode, setViewMode] = useState<'list' | 'map'>('list');
  const [filter, setFilter] = useState<'new' | 'delivered'>('new');
  const [deliveryCounts, setDeliveryCounts] = useState<{
    total: number;
    delivered: number;
  } | null>(null);

  useEffect(() => {
    const fetchCounts = async () => {
      const response = await deliveryService.fetchDeliveryCounts();
      setDeliveryCounts(response);
    };
    fetchCounts();
  }, [deliveryService]);

  return (
    <SafeAreaView style={styles.safeArea}>
      <NavigationBar
        deliveryCounts={deliveryCounts}
        viewMode={viewMode}
        onViewModeChange={setViewMode}
        filter={filter}
        onFilterChange={setFilter}
      />
      <Layout style={styles.container}>
        {viewMode === 'list' ? (
          <DeliveryList filter={filter} setDeliveryCounts={setDeliveryCounts} />
        ) : null}
        {viewMode === 'map' ? (
          <DeliveryMap filter={filter} setDeliveryCounts={setDeliveryCounts} />
        ) : null}
      </Layout>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
});

export default HomePage;
