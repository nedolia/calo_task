import deliveryData from '../data/delivery.json';
import {
  Delivery,
  FetchBoundaryDeliveriesResponse,
  FetchDeliveriesResponse,
  FetchDeliveryCountsResponse,
  RegionBoundary,
} from '../types';

/*
  Service used to emulate getting paginated data from server
 */
class DeliveryService {
  newDeliveries: Delivery[];
  deliveredDeliveries: Delivery[];

  constructor() {
    this.newDeliveries = (deliveryData as Delivery[]).filter(
      delivery => delivery.deliveryStatus !== 'delivered',
    );
    this.deliveredDeliveries = (deliveryData as Delivery[]).filter(
      delivery => delivery.deliveryStatus === 'delivered',
    );
  }

  fetchDeliveryCounts = async (): Promise<FetchDeliveryCountsResponse> => ({
    total: this.newDeliveries.length + this.deliveredDeliveries.length,
    delivered: this.deliveredDeliveries.length,
  });

  fetchDeliveries = async (
    filter: 'new' | 'delivered',
    from: number,
    limit: number,
  ): Promise<FetchDeliveriesResponse> => {
    const deliveries =
      filter === 'new' ? this.newDeliveries : this.deliveredDeliveries;
    return {
      data: deliveries.slice(from, from + limit),
      count: deliveries.length,
    };
  };

  fetchDeliveriesInBoundary = async (
    filter: 'new' | 'delivered',
    boundary: RegionBoundary,
  ): Promise<FetchBoundaryDeliveriesResponse> => {
    const deliveries =
      filter === 'new' ? this.newDeliveries : this.deliveredDeliveries;
    return {
      data: deliveries.filter(
        delivery =>
          delivery.lat > boundary.southLat &&
          delivery.lat < boundary.northLat &&
          delivery.lng > boundary.westLng &&
          delivery.lng < boundary.eastLng,
      ),
    };
  };

  editDeliveryStatus = (id: string, status: 'delivering' | 'delivered') => {
    if (status === 'delivered') {
      const deliveryToEdit = this.newDeliveries.find(
        delivery => delivery.id === id,
      );
      if (deliveryToEdit) {
        this.newDeliveries = this.newDeliveries.filter(
          delivery => delivery.id !== id,
        );
        this.deliveredDeliveries = [
          ...this.deliveredDeliveries,
          { ...deliveryToEdit, deliveryStatus: 'delivered' },
        ];
      }
    } else {
      this.newDeliveries = this.newDeliveries.map(delivery => {
        if (delivery.id === id) {
          return { ...delivery, deliveryStatus: status };
        }
        return delivery;
      });
    }
  };
}

export default DeliveryService;
