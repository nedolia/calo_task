import DeliveryService from '../delivery-service';

const deliveryService = new DeliveryService();

const useDeliveryService = () => {
  return deliveryService;
};

export default useDeliveryService;
