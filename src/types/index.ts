export interface Delivery {
  id: string;
  name: string;
  lat: number;
  lng: number;
  address: string;
  deliveryStatus?: 'delivering' | 'delivered';
}

export interface FetchDeliveriesResponse {
  data: Delivery[];
  count: number;
}

export interface FetchBoundaryDeliveriesResponse {
  data: Delivery[];
}

export interface FetchDeliveryCountsResponse {
  total: number;
  delivered: number;
}

export interface RegionBoundary {
  westLng: number;
  eastLng: number;
  northLat: number;
  southLat: number;
}
